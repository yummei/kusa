# This file tweak dolphin

## What is change.
### status bar
- Disable status bar space slider
- Disable status bar zoom slider
### toolbar
- Disable Split view button
- Add tab button
- Change three view mode to one button
- Change button order
- Change some button not include text
- Disable context menu items:
  - link to activities
  - open in split view
  - duplicate here
  - sort by
  - view mode
  - copy location

## What is can not change.
- Change context menu items name:
  - "create new" change to "create" or "new"
  - "create new" - "[something] file" change to "[something]"
  - "open folder with" change to "open with" (your user know this is a folder)
  - "paste [something]" change to "paste"
  - "open terminal here" change to more clear text

- Redesign no tips items "Press Shift for more info"
- Address bar add "add in places" option
- Address bar ">" contrast more readable like nautilus
- Add nautilus like keyboard shortcuts page
- create file not popup window (do not change your user's attention)
- Rename file inline more readable, you can not know this file is in edit now
- Change context menu items line spacing
- Change places sidebar category and items align more spacing
