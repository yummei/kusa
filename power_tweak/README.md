# This file tweak power settings

## What is change.
### launcher menu
- disable show action button captions
- power setting buttons now hide in secondary menu
- disable logout confirmation screen

## You need to do.
- copy .config to your home
- run ```cat "plasma-org.kde.plasma.desktop-appletsrc" >> "$HOME/.config/plasma-org.kde.plasma.desktop-appletsrc"```
