# Kusa
![kusa](./icons/kusa.svg)

Adjust KDE6 some user friendly. Use Haru design.  
Make commonly used actions visible(not hide in three level menu), hide uncommonly used actions.
![kusa_demo](./icons/kusa_demo.png)

## How to use.
Copy you want to use application to your home and relogin.  

## Adjust application.
* [dolphin](./dolphin)
* [kconsole](./kconsole)
* [taskbar](./taskbar)
* [power button](./power_tweak)

## Can not adjust.
* Settings
We want to KDE rewrite a simple settings like gnome, do not delete previous super settings.  
* Dialog
We not requir every dialog has bottom bar, with OK, Cancel, Apply.  
We need decrease secondary menu interference(context menu, menu)  

## We used project.
* [icon](https://www.iconpacks.net/free-icon-pack/nature-83.html)
* [Haru](https://gitlab.com/yummei/kusa)  
